<?php

namespace App\Interfaces;

interface DownloadInterface
{
    public function saveFile($file, string $path): string;
}